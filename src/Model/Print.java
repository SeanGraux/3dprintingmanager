package Model;

import java.util.Date;

public class Print {

    private Gcode gcode;
    private Filament filament;

    private double filamentCost;
    private double electricityCost;
    private double totalCost;

    private Date datePrint;

    public Print(Gcode gcode, Filament filament) {
        this.gcode = gcode;
        this.filament = filament;

        double kWhCost = 0.1580;
        this.electricityCost = this.gcode.getHours() * kWhCost + this.gcode.getMinutes() * (kWhCost/60) + this.gcode.getSeconds() * (kWhCost/3600);
        this.filamentCost = this.filament.getCostPerKg() * this.gcode.getUsedKg();
        this.totalCost = filamentCost + electricityCost;
        this.printGcode();
    }

    public void printGcode(){
        this.datePrint = new Date();
        this.useFilament(this.gcode.getUsedKg());
    }

    public void useFilament(double filamentKgToBeUsed){
        double filamentKg = this.filament.getCurrentKg();
        if ( filamentKg - filamentKgToBeUsed < 0)
            this.filament.setCurrentKg(0);
        else
            this.filament.setCurrentKg(filamentKg - filamentKgToBeUsed);
    }

    @Override
    public String toString() {
        return "Print{" +
                "gcode=" + gcode +
                ", filament=" + filament +
                ", filamentCost=" + filamentCost +
                ", electricityCost=" + electricityCost +
                ", totalCost=" + totalCost +
                '}';
    }

    public Gcode getGcode() {
        return gcode;
    }

    public void setGcode(Gcode gcode) {
        this.gcode = gcode;
    }

    public Filament getFilament() {
        return filament;
    }

    public void setFilament(Filament filament) {
        this.filament = filament;
    }

    public double getFilamentCost() {
        return filamentCost;
    }

    public void setFilamentCost(double filamentCost) {
        this.filamentCost = filamentCost;
    }

    public double getElectricityCost() {
        return electricityCost;
    }

    public void setElectricityCost(double electricityCost) {
        this.electricityCost = electricityCost;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
}
