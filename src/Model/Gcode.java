package Model;

import java.io.*;
import java.util.Date;
import java.util.StringTokenizer;

public class Gcode {

    private String name;

    private double usedKg;
    private String usedLength;

    private int hours;
    private int minutes;
    private int seconds;

    private double electricityCost;

    private Date dateAdd;

    public Gcode(String pathToGcode){
        String currentLine;
        try {
            FileReader reader = new FileReader(pathToGcode);
            BufferedReader bufferedReader = new BufferedReader(reader);
            while ((currentLine = bufferedReader.readLine()) != null){
                if (currentLine.startsWith("; estimated printing time"))
                    cutLineBuildTime(currentLine);
                else if (currentLine.startsWith("; filament used"))
                    cutLineUsedFilament(currentLine);
            }
        }
        catch (FileNotFoundException e){
            System.out.println("File not Found");
        }
        catch (IOException e){
            System.out.println("IOException");
        }
    }

    public void cutLineBuildTime(String stringToCut){
        StringTokenizer tokenizer = new StringTokenizer(stringToCut);
        int count = 0;
        String tempString[];
        while(tokenizer.hasMoreTokens()){
            if (count == 7){
                tempString = tokenizer.nextToken().split("h");
                this.hours = Integer.parseInt(tempString[0]);
            }
            else if (count == 8){
                tempString = tokenizer.nextToken().split("m");
                this.minutes = Integer.parseInt(tempString[0]);
            }
            else if (count == 9){
                tempString = tokenizer.nextToken().split("s");
                this.seconds = Integer.parseInt(tempString[0]);
            }
            else tokenizer.nextToken();
            count++;
        }
    }

    public void cutLineUsedFilament(String stringToCut){
        StringTokenizer tokenizer = new StringTokenizer(stringToCut);
        int count = 0;
        String tempString ="";
        while(tokenizer.hasMoreTokens()){
            if (count == 4){
                tempString = tokenizer.nextToken();
            }
            else tokenizer.nextToken();
            count++;
        }
        if (count == 5) {
            this.usedKg = Double.parseDouble(tempString) * 0.001;
        }
        else this.usedLength = tempString;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUsedKg() {
        return usedKg;
    }

    public void setUsedKg(double usedKg) {
        this.usedKg = usedKg;
    }

    public String getUsedLength() {
        return usedLength;
    }

    public void setUsedLength(String usedLength) {
        this.usedLength = usedLength;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSecondes(int seconds) {
        this.seconds = seconds;
    }

    public double getElectricityCost() {
        return electricityCost;
    }

    public void setElectricityCost(double electricityCost) {
        this.electricityCost = electricityCost;
    }
}
