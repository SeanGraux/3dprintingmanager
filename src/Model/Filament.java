package Model;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class Filament {

    private String matter;
    private String brand;
    private double costPerKg;

    private String color;

    private double baseKg;
    private double currentKg;

    private String notes;
    private Date dateAdd;

    public Filament(String matter, String brand, String color, double baseKg, double currentKg, String notes, double costPerKg) {
        this.matter = matter;
        this.brand = brand;
        this.color = color;
        this.baseKg = baseKg;
        this.currentKg = currentKg;
        this.notes = notes;
        this.costPerKg = costPerKg;
    }

    public Filament(String matter, String brand, String color, double baseKg, double currentKg, double costPerKg) {
        this.matter = matter;
        this.brand = brand;
        this.color = color;
        this.baseKg = baseKg;
        this.currentKg = currentKg;
        this.costPerKg = costPerKg;
        saveFilament();
    }

    public void saveFilament(){
        try{
            String stringToWrite = this.matter + " - " + this.brand + " - " + costPerKg + " - " + this.color + " - " + baseKg + " - " + currentKg + " - " + this.notes + " - " + this.dateAdd + "\n";
            BufferedWriter writer  = new BufferedWriter(new FileWriter("C:\\Users\\Sean\\Documents\\ProjetPerso\\3dprintingmanager\\Filament.txt", true));

            writer.append(stringToWrite);
            writer.close();
        }
        catch (IOException e){}
    }

    public String getMatter() {
        return matter;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public double getBaseKg() {
        return baseKg;
    }

    public double getCurrentKg() {
        return currentKg;
    }

    public String getNotes() {
        return notes;
    }

    public Double getCostPerKg(){return costPerKg;}

    public void setMatter(String matter) {
        this.matter = matter;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }


    public void setColor(String color) {
        this.color = color;
    }

    public void setBaseKg(double baseKg) {
        this.baseKg = baseKg;
    }

    public void setCurrentKg(double currentKg) {
        this.currentKg = currentKg;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setCostPerKg(double cost){this.costPerKg = cost;}
}
