package Test;

import Model.Filament;
import Model.Gcode;
import Model.Print;

public class TestPrint {

    public static void main(String [] args){
        Gcode testCode = new Gcode("C:/Users/Sean/Documents/3D/Projet/Nerf/FirstTest/BarrelV1_0.2mm_PET_MK2S.gcode");
        Filament filament = new Filament("PLA", "Surreal", "Red", 1, 1, 19.99);

        System.out.println(filament.getCurrentKg());
        Print newPrint = new Print(testCode, filament);
        System.out.println(newPrint.toString());
        System.out.println(newPrint.getFilament().getCurrentKg());
    }
}
