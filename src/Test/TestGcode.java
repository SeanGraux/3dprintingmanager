package Test;

import Model.Gcode;

public class TestGcode {

    public static void main(String [] args){
        Gcode testCode = new Gcode("C:/Users/Sean/Documents/3D/Projet/Nerf/FirstTest/BarrelV1_0.2mm_PET_MK2S.gcode");
        System.out.println(testCode.getHours() + "h " + testCode.getMinutes() + "m " + testCode.getSeconds() + "s");
        System.out.println(testCode.getUsedLength() + " --- " + testCode.getUsedKg());
        System.out.println(testCode.getElectricityCost());
    }
}
